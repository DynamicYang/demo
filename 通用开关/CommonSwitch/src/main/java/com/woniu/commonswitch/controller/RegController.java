package com.woniu.commonswitch.controller;

import com.woniu.commonswitch.service.RegService;
import com.woniu.commonswitch.util.Result;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 程序员蜗牛
 * 教你一招 SpringBoot + 自定义注解 + AOP 打造通用开关
 * </p>
 */
@RestController
@RequestMapping("/api/reg")
@AllArgsConstructor
public class RegController {

	private final RegService regService;

	@GetMapping("/createOrder")
	public Result createOrder() {

		return regService.createOrder();
	}
}
