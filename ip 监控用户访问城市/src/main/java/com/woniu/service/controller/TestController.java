package com.woniu.service.controller;

import com.woniu.service.ipregion.Ip;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * SpringBoot整合ip2region实现使用ip监控用户访问城市
 * 公众号：程序员蜗牛g
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/hello")
    @Ip
    public String hello() {
        return "hello";
    }
}