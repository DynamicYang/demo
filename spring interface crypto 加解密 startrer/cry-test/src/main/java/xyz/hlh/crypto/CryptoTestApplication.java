package xyz.hlh.crypto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wn
 * @description: 启动类
 */
@SpringBootApplication
public class CryptoTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(CryptoTestApplication.class, args);
    }
}
