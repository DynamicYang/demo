package com.woniu.logger;


import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.toolkit.JdbcUtils;

import com.woniu.logger.conf.LoggerConfig;
import com.woniu.logger.sql.DdlSqlFactory;
import com.woniu.logger.sql.IDdlSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 功能描述: 日志信息启动执行 <br/>
 * 日志备份，创建日志表等操作
 */
@Component
public class LoggerInfoApplicationListener implements CommandLineRunner {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private LoggerConfig loggerConfig;
    private IDdlSql ddl;

    @Override
    public void run(String... args) throws Exception {
        // 判断数据库类型
        Connection conn = dataSource.getConnection();
        try (Statement statement = conn.createStatement()) {
            DbType dbType = JdbcUtils.getDbType(conn.getMetaData().getURL());
            ddl = DdlSqlFactory.valueOf(dbType.name()).getDdl();
            // 查询表有没有存在
            if (!existTable(statement)) {
                createTable(statement);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 功能描述: 建表 <br/>
     */
    private void createTable(Statement statement) throws SQLException {
        statement.execute(ddl.createTable());
    }


    /**
     * 功能描述: 是否存在表 <br/>
     */
    private boolean existTable(Statement statement) throws SQLException {
        ResultSet resultSet = statement.executeQuery(ddl.queryTable(""));
        resultSet.next();
        return resultSet.getInt(1) == 1;
    }

}
