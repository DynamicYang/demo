package com.woniu.req;


import lombok.Data;

@Data
public class RequestData<T> {

    private Header header;

    private T body;

}