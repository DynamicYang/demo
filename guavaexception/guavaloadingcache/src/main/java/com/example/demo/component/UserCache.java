package com.example.demo.component;


import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class UserCache {
    // 假设有一个用户服务，用于获取用户信息
    private static UserService userService = new UserService();
    public static void main(String[] args) throws Exception {
        CacheLoader<String, User> loader = new CacheLoader<String, User>() {
            @Override
            public User load(String userId) throws Exception {
                    return userService.getUserById(userId);
            }
        };
        // 创建LoadingCache
        LoadingCache<String, User> cache = CacheBuilder.newBuilder()
                .maximumSize(100) // 设置最大缓存数
                .build(loader);
        // 使用缓存获取用户信息
        User user = cache.get("123"); // 如果缓存中没有，会调用load方法加载数据
        System.out.println(user);
    }
}
