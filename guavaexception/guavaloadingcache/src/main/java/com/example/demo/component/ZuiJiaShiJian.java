package com.example.demo.component;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.TimeUnit;

/**
 * 缓存使用最佳实践
 */
public class ZuiJiaShiJian {

    public static void main(String[] args) {
        LoadingCache<String, String> cache = CacheBuilder.newBuilder()
                .maximumSize(1000) // 设置最大缓存项为1000 合理配置缓存大小
                .expireAfterWrite(10, TimeUnit.MINUTES) // 写入后10分钟过期  设置合适的过期策略
                .expireAfterAccess(5, TimeUnit.MINUTES) // 访问后5分钟过期 设置合适的过期策略
                .softValues() // 使用软引用存储值
                .weakKeys() // 使用弱引用存储键
                .removalListener(notification -> {
                    // 监听移除通知
                    System.out.println("Removed: " + notification.getKey() + ", Cause: " + notification.getCause());
                })
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        try {
                            //   异常处理策略
                            return fetchData(key);
                        } catch (Exception e) {
                            // 处理异常
                            return null;
                        }
                    }
                    private String fetchData(String key) {
                        throw new RuntimeException();
                    }
                });



    }
}
