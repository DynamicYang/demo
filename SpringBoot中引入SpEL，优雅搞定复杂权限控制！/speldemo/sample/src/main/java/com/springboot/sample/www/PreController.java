package com.springboot.sample.www;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/pre")
@Slf4j
//@PreAuth(value = "hasRole('admin')")
public class PreController {

    /**
     * SpringBoot中引入SpEL，优雅搞定复杂权限控制！
     * @param userId
     * @return
     */
    @GetMapping("/{userId}")
    @ResponseBody
    @PreAuth(value = "hasRole('admin')")
    public String getUser(@PathVariable String userId) {
        log.info("hahaha");
        return userId;
    }
}
