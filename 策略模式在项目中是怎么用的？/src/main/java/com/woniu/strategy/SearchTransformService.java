package com.woniu.strategy;

import cn.hutool.core.lang.Assert;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

//知道策略模式！但不会在项目里使用？
@Service
public class SearchTransformService implements InitializingBean {

//    @Autowired
//    private UserTransferService userTransferService;

//    @Autowired
//    private AgeTransferService ageTransferService;

//    @Autowired
//    private InterestTransferService interestTransferService;

    /**
     * 根据不同的编码进行转换
     * @param code
     * @return
     */
    public String transform2(String code) {
//        if(code.equals("user")){
//            return userTransferService.transfer();
//        }else if(code.equals("age")){
//            return ageTransferService.transfer();
//        }else if(code.equals("interest")){
//            return interestTransferService.transfer();
//        }
        return "";
    }

    @Autowired
    private List<TransferService> transferServiceList;

    private Map<CodeEnum, TransferService> transferServiceMap;

    @Override
    // 项目启动时将实现类放入到map中去
    public void afterPropertiesSet() throws Exception {
        transferServiceMap = transferServiceList
                .stream()
                .collect(Collectors.toMap(TransferService::transCode, Function.identity()));
    }

    /**
     * 根据不同的编码进行转换
     * @param code
     * @return
     */
    public String transform(String code) {
        TransferService transferService = transferServiceMap.get(CodeEnum.of(code));
        Assert.notNull(transferService, "找不到对应的转换器");
        return transferService.transfer();
    }

}
