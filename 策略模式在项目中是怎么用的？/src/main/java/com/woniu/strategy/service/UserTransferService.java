package com.woniu.strategy.service;

import com.woniu.strategy.CodeEnum;
import com.woniu.strategy.TransferService;
import org.springframework.stereotype.Service;

@Service
public class UserTransferService implements TransferService {
    @Override
    public String transfer() {
        return "user";
    }

    @Override
    public CodeEnum transCode() {
        return CodeEnum.USER;
    }
}
