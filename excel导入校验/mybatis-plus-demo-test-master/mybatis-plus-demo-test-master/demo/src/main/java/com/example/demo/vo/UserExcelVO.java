package com.example.demo.vo;

import com.example.demo.entity.UserExcel;
import lombok.Data;

import java.util.List;

@Data
public class UserExcelVO {
    /**
     * 成功列表
     */
    private List<UserExcel> success;
    /**
     * 失败列表
     */
    private List<UserExcel> fail;

}
